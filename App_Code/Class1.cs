﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;


/// <summary>
/// Summary description for Class1
/// </summary>
public static class Class1
{
    public static void createRandomList(this ListBox lbx)
    {
        lbx.Items.Clear();
        //Xóa nội dung trong ListBox
        Random rd = new Random();
        for (int i = 0; i < 30; i++)
            lbx.Items.Add(rd.Next(-100, 100)+"");
        //Tạo danh sách ngẫu nhiên và add vào ListBox
    }

    public delegate bool delegateCheck(int num);
    //Mặt nạ hàm dùng để kiểm tra số nguyên.
    //Hàm nào chứa tham số là mặt nạ này,
    //thì các yêu cầu không cần đưa vào từ đầu,
    //mà chỉ cần đưa vào khi gọi hàm mà thôi.
    public static List<int> getList(this ListBox lbx, delegateCheck check)
    {
        List<int> list = new List<int>();
        for (int i=0; i<lbx.Items.Count; i++)
        {
            int x = int.Parse(lbx.Items[i].Text);
            if(check(x))
                list.Add(x);
            //Mặt nạ hàm được đưa vào hàm if, 
            //để khi gọi hàm getList từ ListBox,
            //ta phải đưa vào yêu cầu kiểm tra số nguyên,
            //nếu đúng yêu cầu thì số nguyên đó mới được add vào List.
            //Mặt nạ hàm giúp ta đưa các yêu cầu vào sau,
            //để ko cần tạo ra quá nhiều hàm tương tự nhau.
        }
        return list; 
    }

    public static List<int> getCal(this List<int> list, string sumOrCount)
    {
        int cal = 0;
        foreach (int i in list)
        {
            if (sumOrCount.Equals("sum"))
                cal += i;
            else if (sumOrCount.Equals("count"))
                cal += 1;
        }
        list.Clear();
        list.Add(cal);
        return list;
    }

    public static void setSortAndCheck(this ListBox lbx, 
        List<int> list, string desOrAsc, delegateCheck check)
    {
        if (desOrAsc.Equals("asc"))
        {
            var sortedList = list
                            .Where(x => check(x))
                            .OrderBy(x => x);
            lbx.DataSource = sortedList;
            lbx.DataBind();
        }
        else if (desOrAsc.Equals("desc"))
        {
            var sortedList = from n in list
                             where check(n)
                             orderby n descending
                             select n;
            lbx.DataSource = sortedList;
            lbx.DataBind();
        }
    }
}