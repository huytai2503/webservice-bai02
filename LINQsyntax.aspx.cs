﻿using System;
using System.Collections.Generic;

public partial class LINQsyntax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCreateList_Click(object sender, EventArgs e)
    {
        lbxSource.createRandomList();
        //Tạo danh sách số ngẫu nhiên cho chính ListBox gọi hàm

        List<int> sumAllList = lbxSource.getList(x => x / x == 1);
        //Hàm getList trả về một danh sách lấy từ ListBox gọi nó.
        //Khi gọi hàm getList, ta phải đưa vào yêu cầu kiểm tra,
        //vì hàm này có chứa tham số là mặt nạ hàm (delegate),
        //nếu đáp ứng yêu cầu, thì số đó mới được lấy ra.
        sumAllList = sumAllList.getCal("sum");
        //Hàm getCal chỉ có một tham số string, 
        //là yêu cầu tính tổng hay đếm tổng.
        //hàm lấy giá trị từ List<> gọi nó,
        //và trả về một danh sách dựa theo từ tham số đưa vào,
        foreach (int i in sumAllList)
            lblSumAll.Text = "Tổng tất cả.... = " + i;

        List<int> sumEvenList = lbxSource.getList(x => x % 2 == 0);
        sumEvenList = sumEvenList.getCal("sum");
        foreach (int i in sumEvenList)
            lblSumEven.Text = "Tổng số chẵn. = " + i;

        List<int> sumOddList = lbxSource.getList(x => x % 2 != 0);
        sumOddList = sumOddList.getCal("sum");
        foreach (int i in sumOddList)
            lblSumOdd.Text = "Tổng số lẻ..... = " + i;

        List<int> countAllList = lbxSource.getList(x => x / x == 1);
        countAllList = countAllList.getCal("count");
        foreach (int i in countAllList)
            lblCountAll.Text = "Đếm tất cả..... = " + i;

        List<int> countEvenList = lbxSource.getList(x => x % 2 == 0);
        countEvenList = countEvenList.getCal("count");
        foreach (int i in countEvenList)
            lblCountEven.Text = "Đếm số chẵn. = " + i;

        List<int> countOddList = lbxSource.getList(x => x % 2 != 0);
        countOddList = countOddList.getCal("count");
        foreach (int i in countOddList)
            lblCountOdd.Text = "Đếm số lẻ...... = " + i;
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        List<int> list = lbxSource.getList(z => z / z == 1);
        string chosenSort = RadioSortList.SelectedValue;
        switch (RadioTypeList.SelectedValue)
        {
            case "tatCa":
                lbxTarget.setSortAndCheck(list, chosenSort, y => y / y == 1);
                lblDetial.Text = "Danh sách tất cả";
                break;
            case "soChan":
                lbxTarget.setSortAndCheck(list, chosenSort, y => y % 2 != 0);
                lblDetial.Text = "Danh sách số lẻ";
                break;
            case "soLe":
                lbxTarget.setSortAndCheck(list, chosenSort, y => y % 2 == 0);
                lblDetial.Text = "Danh sách số chẵn";
                break;
        }
        //Danh sách switch case này được chọn từ một RadioButtonList,
        //value của RadioButtonList được thiết lập giá trị theo ý mình.
    }
}