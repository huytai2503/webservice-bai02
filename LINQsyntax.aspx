﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LINQsyntax.aspx.cs" Inherits="LINQsyntax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 38px;
            width: 331px;
        }
        .auto-style3 {
            height: 38px;
            width: 4px;
        }
        .auto-style5 {
            height: 38px;
            width: 101px;
        }
        .auto-style6 {
            width: 101px;
        }
        .auto-style7 {
            width: 331px;
        }
        .auto-style8 {
            width: 4px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">
                    <asp:Button ID="btnCreateList" runat="server" Text="Tạo danh sách" OnClick="btnCreateList_Click" />
                </td>
                <td class="auto-style5">
                    <asp:Button ID="btnConfirm" runat="server" Text="Chọn lọc" OnClick="btnConfirm_Click" />
                </td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style8">
                    <br />
                    <asp:ListBox ID="lbxSource" runat="server" Height="520px" Width="145px"></asp:ListBox>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="lblDetial" runat="server" ForeColor="#FF3300" Text="Chi tiết"></asp:Label>
                    <br />
                    <asp:ListBox ID="lbxTarget" runat="server" Height="520px" Width="145px"></asp:ListBox>
                </td>
                <td class="auto-style7">
                    <asp:RadioButtonList ID="RadioSortList" runat="server" Height="16px" Width="153px">
                        <asp:ListItem Selected="True" Value="asc">Tăng dần</asp:ListItem>
                        <asp:ListItem Value="desc">Giảm dần</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                    <asp:RadioButtonList ID="RadioTypeList" runat="server" Height="16px" Width="216px">
                        <asp:ListItem Selected="True" Value="tatCa">DS Tất cả</asp:ListItem>
                        <asp:ListItem Value="soLe">DS Số lẻ</asp:ListItem>
                        <asp:ListItem Value="soChan">DS Số chẵn</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                    <asp:Label ID="lblSumAll" runat="server" Text="Tổng tất cả"></asp:Label>
                    <br />
                    <asp:Label ID="lblSumEven" runat="server" Text="Tổng số chẵn"></asp:Label>
                    <br />
                    <asp:Label ID="lblSumOdd" runat="server" Text="Tổng số lẻ"></asp:Label>
                    <br />
                    <asp:Label ID="lblCountAll" runat="server" Text="Đếm tất cả"></asp:Label>
                    <br />
                    <asp:Label ID="lblCountEven" runat="server" Text="Đếm số chẵn"></asp:Label>
                    <br />
                    <asp:Label ID="lblCountOdd" runat="server" Text="Đếm số lẻ "></asp:Label>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
